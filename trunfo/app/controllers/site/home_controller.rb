class Site::HomeController < ApplicationController
before_action :authenticate_user!
include ApplicationHelper

layout "site"
  def index
    @decks = Deck.all
    @cards = Card.all
    @games = Game.all
    @deck_cards = DeckCard.all
    @players = Player.all
    @myowngames = @games.where(user: current_user)
    @opengames = @games.select { |game| game.nplayer > number_of_players(game) }
    @myplayers = @players.where(user:current_user)
     @mydecks = @decks.where(player:@myplayers)
     @mydeck_cards = @deck_cards.where(deck:@mydecks)
    # raise
    mygames = @myplayers.map{|player| player.game_id}
    # @myplayers.each do |player|
      @mygames =  @games.where(id: mygames)

    # end


# without the select...
# @games.each do |game|
#   puts "#{game.nplayer} > #{number_of_players(game)}"
#   if game.nplayer > number_of_players(game)
#         @opengames.push game
#   end
# end
  end


end
