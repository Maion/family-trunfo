#require "deck"
module GameFamilyTrunfo
	# This class is the main class to shuffle a deck of cards
	# @author Felipe Maion
	
	class Game
		attr_reader :players, :cards_per_player,  :cards_collection, :maximun_cards_per_player, :player
		attr_accessor :cards_availables
		
		# the minimun cards per player
		MINIMUN_CARDS = 1
		# the minimun players in a game
		MINIMUN_PLAYERS= 2
		# the default value of players
		PLAYERS = 2
		# Count total cards in a deck
		TOTAL_CARDS = Deck.new.cards.count 
		# Maximun players allowed in a Game
		MAXIMUN_PLAYERS = TOTAL_CARDS # the maximun players in a game

		def initialize(players = PLAYERS, params ={} )
		
		# the defaul value of cards per player
		# Validate the input params and parse values
			cards_per_player = ( TOTAL_CARDS / players )
			@players, @cards_per_player = ValidateGame.parse_and_validate(players,cards_per_player)

			# The card collection, all the cards in the deck
			@cards_collection = {}
			puts "Creating the Deck..."
			@cards_collection = Deck.new.cards
			
			@cards_availables= @cards_collection
		 # params.each { |key,value| instance_variable_set("@#{key}", value) }
		# # set_defaults
		 # instance_variables.each {|var| self.class.send(:attr_accessor, var.to_s.delete('@'))}

			# Initialize the maximun possible of cards per player,
			# given a card collection and a number of players
			@maximun_cards_per_player = Game.get_maximun_cards_per_player(@players)

		end

  
		# Shuffle the deck of cards
		# @return a group of players [Hash] each with their cards
		def results
			players={}
			@cards_availables.shuffle!
			# in_groups is defined in array.rb (no need for it on Rails ):
			cards =  @cards_availables.in_groups(@players,false).to_a 
			@players.times do |player|
				players[player]= cards[player]
				@cards_availables -= cards
			end
		return players
		end

		
		def total_cards
		TOTAL_CARDS
		end
		
		# Get the maximun of cards per player,
		# given a card collection and a number of players
		# @return maximun cards per player [Integer]
		def self.get_maximun_cards_per_player(players)
			players = ValidateGame.parse_and_validate_players(players)
			( TOTAL_CARDS / players ) 
		end
	end
end