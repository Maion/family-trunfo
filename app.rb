# FAMILY TRUNFO (ACE TRUMP or TOP TRUMP like) 
# Author: Felipe Maion - co-author: Pedro "Peter" Boldo Garcia. - Maion & Sys.
# Maionesys (r) - An Emulsification of Bits!

# Idea: Family Trunfo
# Game: A deck of cards is shuffled and is dealed for each players (min: two ||= AI).
# Once shuffled, cards order cannot be changed.
# So, each player will have a smaller (1/n, n= 2) deck of cards. 
# each card has one Family member. With its stats.

# The initial player (player 0 or random... ) start asking one of the stats from his card.
# The card that has the best (usually greater) value for this stat wins the card. 
# Looses the player without a card.
# The winned cards goes to the end of the winning player's deck.
# The winner asks the attribute in the next turn.
# Wins the player that has all the cards

# # # # 1. Observe, para o Family Trunfo escolhido, qual valor vence em cada característica: se o menor ou o maior valor.

# # # # 2. Para iniciar, escolha entre as informações da sua carta, aquela que você julga ter o valor capaz de vencer as cartas dos seus adversários. 
# # # # Exemplo: Se você escolher a informação “velocidade”, clique sobre o campo “km/h”. As cartas dos seus adversários se abrirão, mostrando quem venceu. Compare os resultados e clique em PRÓXIMA CARTA.
# # # # •se você vencer - as cartas dos outros jogadores irão para trás do seu monte de cartas e você continua escolhendo a informação da sua próxima carta.
# # # # •se outro jogador vencer - as cartas irão para trás do monte de cartas dele e a vez de escolher passa para ele. Em sua tela aparecerá a mensagem de AGUARDE, até que o jogador que venceu escolha a característica que ele deseja para a nova carta.
# # # # •em caso de empate - as cartas irão para o monte e as próximas cartas - sua e dos seus adversários - aparecem. Uma nova disputa é feita sendo que o jogador que escolheu as cartas que empataram deve escolher novamente. Neste caso, apenas os jogadores que empataram jogam novamente para ver quem será o vencedor. O vencedor ganha as cartas que empataram e estão no monte. Se um dos jogadores que empataram não possuir outra carta para jogar, a anterior volta para a mão dele para ser usada no desempate.

# # # # 7. CARTA SUPER TRUNFO
# # # # Existe entre as cartas uma carta SUPER TRUNFO. Esta carta vence todas as cartas do baralho independentemente do valor de suas características. Ela perde apenas para as cartas que tenham a letra A (1A, 2A, 3A, etc), marcado na parte superior de cada uma.

# # # # 8. Quando a carta SUPER TRUNFO aparecer, a comparação será automática com as cartas dos adversários, sem necessidade de você escolher uma característica de sua carta. 

# # # # 9. FIM DO JOGO
# # # # O jogo termina quando um dos jogadores ganhar todas as cartas do baralho.

require "version"
include GameFamilyTrunfo
require 'pp'

players = []
puts "Creating game..."
game = Game.new(4)
puts "Game created for #{game.players} players"

game_over = false 
game_winner = nil 
game_tied = []

puts "Dealing cards for each player..."
players_hand = game.results
total_cards = game.total_cards
players_hand.each {|player, hand| players[player] = hand}

## HERE THE GAME STARTS AMONG PLAYERS:
game_counter = 0
player_turn = 0
while !game_over


	#lets see the hand
	puts "Checking the hands of each player..."
	current_player = 0
	players.each do |player|
	puts "Player " + current_player.to_s
	current_player += 1 
	pp player
	end
	print "Player[#{player_turn}] asks attribute:"
	attribute = gets.chomp #"@altura"
	if attribute == "" then attribute = "@name" end #picked @name because of high probability of tied game.
	 
	
# # # # attribute = gets.chomp #"@altura"
# # INTERACTION?	
	# get top cards (for each playes) # Who needs to be dry?
	# get the attribute value
	# compare the attribute, and determine the winner.

	## PLAYER #n REQUEST ATTRIBUTE!
	
	puts "Get the top card from each player's deck and puts on the table"
	current_player = 0
	winner = nil
	top_arg = nil 
	game_tied = []
	current_cards = []
	players.each do |player|
			card =  player.shift
		att = card.instance_variable_get(attribute)
		puts attribute + ": "+ att.to_i.to_s + " from player " + current_player.to_s
		
		#compare cards
		if att.to_i > top_arg.to_i   # what if they are equal??
			winner = current_player 
			top_arg = att
			game_tied = [winner]
			player_turn = winner
		elsif att.to_i == top_arg.to_i # game is tied!
			game_tied.push current_player
		end
		current_player += 1
		current_cards.push card unless card == nil
	end
	
	if game_tied != [winner]  # I bet there is a better way to do this... :/
	game_tied.each {|player| puts "Game tied between #{player}" }
	
	puts "Untying... Fliping the coin..."
	winner = game_tied.shuffle.first
	end
	
	puts "Winner is #{winner}"
	puts "The winner gets all the cards on the table..."
	current_cards.each {|card| players[winner].push card}

	# check if we have a Game winner (or a 'endless' looping game )
	players.each do |player|
	
		 if player.count.to_i == total_cards.to_i  
		  game_winner = winner 	
		   game_over = true
		  break
		  else
		  game_over = false
		end
	
		
	end	
	
	# check if we have an 'endless' looping game 
		#looping endless game ?
	game_counter > 30 ?	players.each {|player| player.shuffle!} : nil 
	# reset hand 
	winner = nil 
	current_cards = []
	game_counter += 1 
	

end

puts
puts "The winner is Player ##{game_winner}! Game Counter: #{game_counter}" 

## So far so good.
# now it is time to make it a web app.







