class DeckCard < ActiveRecord::Base
  belongs_to :deck#, dependent: :destroy # deck belongs_to player
  belongs_to :card#, dependent: :destroy
  
  validates_uniqueness_of :card, scope: :deck



end
