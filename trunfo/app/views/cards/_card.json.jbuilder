json.extract! card, :id, :name, :description, :height, :birth, :jump, :trunfo, :imune, :created_at, :updated_at
json.url card_url(card, format: :json)