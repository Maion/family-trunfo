# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# Create User at DB
puts "Creating User"
puts User.create ({email:"felipe.maion@gmail.com", password:"123456", password_confirmation:"123456"})
puts User.create ({email:"user@felipemaion.com", password:"1234567", password_confirmation:"1234567"})
puts User.create ({email:"user1@akak.com", password:"1234567", password_confirmation:"1234567"})
puts User.create ({email:"user2@aaaa.com", password:"1234567", password_confirmation:"1234567"})
puts User.create ({email:"user3@aaaa.com", password:"1234567", password_confirmation:"1234567"})

puts "Creating Game"
#Create Game at DB
			game_list = [
				{description:"Game 01", user: User.first, nplayer: 2},
				{description:"Game 02", user: User.second, nplayer: 2},
				{description:"Game 03", user: User.second, nplayer: 2}
								]

				game_list.each do |game|
				p	Game.create (game)
				end


# Create Players at DB. TODO: Do I need the Player to be Devise?? User is already... hummm may be not...
puts "Creating Player"
			players_list = [
				{name: "Player 1",user: User.first, game:Game.first},
				{name: "Player 2", user: User.first, game:Game.second},
				{name: "Player 3", user: User.second, game:Game.first},
				{name: "Player 4", user: User.third, game:Game.first},
				{name: "Player 5", user: User.fourth, game:Game.second}
				]


				players_list.each do |player|
				puts	Player.create! (player)
					puts " - Player: #{player} cadastrado."
				end
#puts "Players cadastrados."

# Create Cards at DB
puts "Creating Card"
			cards_list = [
							{name:"Tio Pipo", height: 187, jump: 31, birth:"03/04/1985", imune:true, trunfo:true},
							{name:"Tio Bru", height: 187, jump: 36},
							{name:"Pedroca", height: 130, jump: 5},
							{name:"VovoTaba",height: 185, jump: 60},
							{name:"Tia Mimi", height: 167, jump: 33},
							{name:"Tia Fer", height: 174, jump: 29},
							{name:"Vovo Vivi", height: 170, jump: 59},
							{name:"Tia Lili", height: 172, jump: 34},
							{name:"Tio De", height: 180, jump: 33}
					]

				cards_list.each do |card|
					puts Card.create (card)
				end


# all_cards_in_groups = all_cards.in_groups(Player.all.count).to_a # TODO Player.all.count --> Players of the game...

puts "Creating Deck"
puts " - Deck for Players of Game 1..."
# Who is going to play??
puts game1 = Player.all - [Player.second, Player.last]
# puts "#{game1.count}**************************"
			game1.each do |player|
				deck =	Deck.create ({player: player, game: Game.first})
				#player.update_attribute(:game, deck.game)
			end
			puts Game.first
		Game.first.update_attribute(:nplayer, game1.count.to_i)

puts " - Deck for Players of Game 2..."
		game2 = [Player.second, Player.last]
						game2.each do |player|
							deck = Deck.create ({player: player, game: Game.second})
						#	puts "Player #{player.name} will be at game:#{deck.game.description}"
						#	player.update_attribute(:game, deck.game)
						end
		Game.second.update_attribute(:nplayer, game2.count.to_i)
			# all_cards_in_groups.each do |players_cards|
			#	Player.all[all_cards_in_groups.index(players_cards)]
	  	# all_cards_in_groups =	 Card.all.in_groups(Player.all.count).to_a

# => Deck.all.zip(Card.all.in_groups(Player.all.count))
puts "Creating DeckCard"
puts " - DeckCard for Game 1..."
			decks = Deck.where(game:1)
			 # What cards are you going to use in the Game?
			all_cards = Card.where("height > ?", 179)
	# TODO here is the part where the User can create the Cards and Deck to use @ game..
# Get the desired cards, and group them according to the number of decks (... players seems better... but):
			all_cards_in_groups = all_cards.in_groups(decks.count)
			# # [1,2].zip(['a'..'h', 'i'..'z']) = [1,'a..h'],[2.'i..z']
			deckcard = decks.zip(all_cards_in_groups)
			deckcard.each do |deck, cards|
				cards.compact.each do |card| # all_cards in groups of number of decks... have null? We don't need it..
					puts " -- DeckCard: " +	DeckCard.create({deck: deck, card: card}).to_s + " - Baralho ID:#{deck.id} << Carta ID:#{card.id}"
				end
			end
puts " - DeckCard for Game 2..."
			decks = Deck.where(game:2)
			all_cards_in_groups = Card.all.in_groups(decks.count)
			deckcard = decks.zip(all_cards_in_groups)
			deckcard.each do |deck, cards|
					cards.compact.each do |card|
						puts " -- DeckCard: " +	DeckCard.create({deck: deck, card: card}).to_s + " - Baralho ID:#{deck.id} << Carta ID:#{card.id}"
					end
			end

			puts "----------------------------------------------------------------------"
			puts "Status do DB: "
			puts "Players: #{Player.all.size} - Games: #{Game.all.size} - Cards: #{Card.all.size}"
			puts "Decks  : #{Deck.all.size} - DeckCards: #{DeckCard.all.size}"
			puts "Users  : #{User.all.size}"
			puts "----------------------------------------------------------------------"


			# Let's pretend to play...
			#  game = Game.where({id:1}).first
				Game.all.each do |game|
					decks = Deck.where({game:game})
					puts "#{game.description}: #{decks.count} players"
					# How to get number of players in a Game? decks.count dont seem right...
					#   Now I can game.players.count... nested!
					decks.each do |deck|
						player = deck.player
						player_deck = Deck.where({game:game, player:player})
						card = DeckCard.where(deck:player_deck).first.card # TODO NIL????
						puts "Top Card: #{card.name} - #{card.height} - Player: #{player.name}"
					end
				end



		# 	 players_top_card = DeckCard.where(deck:decks).first
		# 	#  decks.each do |deck|
		# 	#  	players_top_card = DeckCard.where({deck:deck})
		# 	 #  end
		#
		#
		#
		# deck = Deck.where ({game:"1", player:"1"})
		# deck2 = Deck.where ({game:"1", player:"2"})
		#
		# player_cards = DeckCard.where({deck: deck})  # // pegamos as cartas do jogador a partir do deck
		# player2_cards = DeckCard.where({deck: deck2})
		#
		# puts "Last Card: #{player_cards.first.card.name}"
		# puts "Last Card: #{player2_cards.first.card.name}"
		#
		# player_cards.each do |player_card|
		#     puts player_card.card.name  # // listamos as cartas do deck do jogador para determinado game
		# end
