# attr_accessor :name, :date, :type, :asset_id

	  # def initialize(params = {})
	  	# params.each { |key,value| instance_variable_set("@#{key}", value) }
		# set_defaults
		# instance_variables.each {|var| self.class.send(:attr_accessor, var.to_s.delete('@'))}
	  # end

	  # def to_s
		# instance_variables.inject("") {|vars, var| vars += "#{var}: #{instance_variable_get(var)}; "}
	  # end

module GameFamilyTrunfo

	# @author Diego Hernán Piccinini Lagos
	# @modified Felipe Maion
	class Card
		@@count = 0
		# Create a new card
		# @param card_value [String]
		# @param suit [String]
		def initialize( params = {})
		
		@@count += 1
		params.each { |key,value| instance_variable_set("@#{key}", value) }
		# @id ||= card_id + " " + params.to_s 
			
		instance_variables.each {|var| self.class.send(:attr_accessor, var.to_s.delete('@'))}
		
		end
		attr_reader	:id, :card_value, :suit, :image

		# Get a image name file
		# @param id [String]
		# @return a file name [String]
		class << self
			def image(id)
				id.tr(' ','_') + '.png'
			end
		end
		
		def Card.count
		@@count
		end
		
	end
end