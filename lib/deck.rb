#require "card"				

module GameFamilyTrunfo

	# @author Felipe Maion
	
	class Deck < Array # The deck
		attr_accessor :cards # @return [Array<Card>]
		def initialize(args=Hash.new(false))
			@cards = []
					@cards.push Card.new({name:"Tio Pipo",  altura: 187, idade: 31})
					
					@cards.push Card.new({name:"Tio Bru",  altura: 187, idade: 36})
					
					@cards.push Card.new({name:"Pedroca",  altura: 130, idade: 5})
					
					@cards.push Card.new({name:"VovoTaba", altura: 185, idade: 60})
					
					@cards.push Card.new({name:"Tia Mimi",  altura: 167, idade: 33})
					
					@cards.push Card.new({name:"Tia Fer",  altura: 174, idade: 29})
					
					@cards.push Card.new({name:"Vovo Vivi",  altura: 170, idade: 59})
					
					@cards.push Card.new({name:"Tia Lili", altura: 172, idade: 34})
										
					@cards.push Card.new({name:"Tio De", altura: 180, idade: 33})
					# @cards.push Card.new({name:"Tia Vivi", altura: 180})
		end
			
	end
		
		def to_s
		@cards
		end
		
	def Deck(args=Hash.new(false))
		Deck.new args
	end
		
		# private
			# def stock(num, suit=Card::Suit.sample) # Creates a Card to add to Deck#cards
				# @cards.push Card.new num, suit
			# end
end
