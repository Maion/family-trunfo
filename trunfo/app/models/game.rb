class Game < ActiveRecord::Base
    has_many :decks
    has_many :players, through: :decks, dependent: :destroy
    has_many :deck_cards, through: :decks
    has_many :cards, through: :deck_cards
    has_many :messages
    #has_one :deck # I am not sure here!!
    belongs_to :user
    validates_uniqueness_of :description
    validates :nplayer, :numericality => { :greater_than_or_equal_to => 2 }
    accepts_nested_attributes_for :decks, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :deck_cards, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :players, reject_if: :all_blank, allow_destroy: true
    
    
    #@messages = Message.where(game:@game)
    def top_card
     self.cards.first
    end
    
    def play(att)
        @game_tied =[]
       # raise "Hei hei hei" # See how difficult is to spot this in the application??
        puts "+/+/+/+/+/+/+/+/+/+/+ Battle starts... +/+/+/+/+/+/+/+/+/+/+"
        @game = self
         puts "#{@game.description} - class: #{@game.class}"
        @players = Player.where(game: @game)
        table = []
        #pego a primeira carta (e deckcard) para cada player<br> -->
        @players.each do |player|
            #<%= "Player:#{boldme(player)}".html_safe %><br>
            @deck = Deck.where(player:player)
            @deck_cards = DeckCard.where(deck: @deck)
        
            puts "ooooo---------------------- Player: #{player.name} - #{player.deck.cards.first}"
            #card = player.deck.cards.first
            card_id = player.deck.cards.ids.first

            
        #puts "#{card.name}"
            if card_id
                 card = Card.find(card_id)
               deckcard = DeckCard.find_by(deck:Deck.find(player.deck.id), card:card).destroy
              # puts "Card: #{card.name} belongs to Player: #{player.name}"
                table.push [card, deckcard] if card_id
                card_id = nil
                card = nil
            end
        end
        #<% table %> #why? dont remember... needed?

        @winner = Player.new 
        top_arg = 0 
        table.each do |card,deckcard| 
            current_player = deckcard.deck.player 
            puts "Current_player: #{current_player.name}: #{card.name} - arg(#{att}): #{card.attributes[att]}" 
            arg = card.attributes[att]
             
            if arg.to_i > top_arg.to_i 
                @winner = current_player
                top_arg = arg
                @game_tied = [@winner]
                
                @player_turn = @winner
            elsif att.to_i == top_arg.to_i # TODO IMPLEMENT TIED GAME...
                @game_tied.push current_player #tá errado no master! faltou <% %>
            end
        end
        if @game_tied != [@winner]  # I bet there is a better way to do this... :/
    	    @game_tied.each {|player|  Message.create ({content:"Player: #{player.name} is tied", game:@game, player:player})}
    	    Message.create ({content:"I am the last one tied... Untying... Spinning the Wheel...", game:@game, player:@game_tied.last}) 
    	    @winner = @game_tied.shuffle.first # Ok... I can make it better... a new game only with them? TODO
        end
            
            puts "W:#{@winner.name}, #{Player.find(1).name},#{@game.description}"
            won_cards =table.map {|card,deckcard| @winner.deck.cards << card ; "#{card.name}" }.join(', ')
             content = "Player: #{Gon.global.chat_player_name} asked #{att}, and the winner is: #{@winner.name} with #{top_arg} , taking home: #{won_cards}"
          # create will call it again... loop not desireble.
          #   Message.create({content:content, player:Player.find(1), game: @game})
           content
            #player1.deck.cards << deckcard1.card 
            #player1.deck.cards << deckcard2.card 
    end
end
