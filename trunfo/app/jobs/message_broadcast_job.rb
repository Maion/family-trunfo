class MessageBroadcastJob < ApplicationJob
  queue_as :default

  def perform(message)
    puts "===========================#{message.content}--#{message.game.id}"

   ActionCable.server.broadcast("game_channel_#{message.game.id}", render_message(message))

      # render_message(message)
      # Do something later
  end

  private

  
    def render_message(message)
      ApplicationController.renderer.render(partial: 'messages/message', locals:{message:message})
      
    end
  
end
