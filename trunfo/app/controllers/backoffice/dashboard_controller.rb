class Backoffice::DashboardController < ApplicationController
layout "backoffice"

  def index
    @cards = Card.all
    @games = Game.all
    @decks = Deck.all
    @deck_cards = DeckCard.all
    @players = Player.all
    @users = User.all
  end
end
