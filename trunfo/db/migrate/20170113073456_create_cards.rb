class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.string :name
      t.string :description
      t.integer :height
      t.date :birth
      t.integer :jump
      t.boolean :trunfo
      t.boolean :imune

      t.timestamps null: false
    end
  end
end
