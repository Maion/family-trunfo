class Deck < ActiveRecord::Base
  belongs_to :game
  belongs_to :player, dependent: :destroy
  has_many :deck_cards, dependent: :destroy
  has_many :cards, through: :deck_cards#, dependent: :destroy


  validates_uniqueness_of :player, scope: :game
  accepts_nested_attributes_for :deck_cards, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :player, reject_if: :all_blank, allow_destroy: true
  # validates_uniqueness_of :card, scope: :game
end
