json.extract! game, :id, :description, :user_id, :nplayer, :created_at, :updated_at
json.url game_url(game, format: :json)