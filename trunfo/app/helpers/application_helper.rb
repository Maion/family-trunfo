module ApplicationHelper

  def number_of_players (game)
  @decks.where(game:game).count
  end
  def number_of_cards(game)
  @deck_cards.where(deck:@decks.where(game:game)).count
  end

  def boldme(player)
    if player.user_id == current_user.id
    link_to content_tag(:strong, "#{player.name}"), player_path(:id => player.id)
    else
      link_to  "#{player.name}", player_path(:id => player.id)
    end
  end
end
