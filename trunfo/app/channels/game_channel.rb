class GameChannel < ApplicationCable::Channel
  def subscribed
 puts "%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SUBSCRIBED  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
 puts "#{params}"
     stream_from "game_channel_#{params[:game]}" # TODO como definir a sala??
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak (data)
     puts "%%%%%%%%%%%%% SERVER SIDE %%%%%%%%%%%%%%"
 puts "#{params}"
 puts "#{data}"
    ## HERE## Is this supposed to be at message_broadcast_job?
    # ActionCable.server.broadcast "game_channel_#{params[:game]}", data # TODO AQUI.....
    Gon.global.chat_player_id = Player.find(data['player'])
    Gon.global.chat_player_name = Player.find(data['player']).name
    # if message == card.atribute then speak
    # else just display msg!
    player = Player.find(data['player'])
    message = data['message']
    game = Game.find(data['game'])
    
    
    all_cards_attributes = player.deck.cards.first.attributes.keys.map(&:downcase)
    if all_cards_attributes.include? message.downcase
      #if data['player'] == current_player (or winner) TODO
      content = game.play(message.to_s)
      
      Message.create ({content:content, game:game, player:player})
    else
      Message.create ({content:message, game:game, player:player})
    end
  end
  

  
end
