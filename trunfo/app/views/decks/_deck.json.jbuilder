json.extract! deck, :id, :game_id, :player_id, :description, :created_at, :updated_at
json.url deck_url(deck, format: :json)