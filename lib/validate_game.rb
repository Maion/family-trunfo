
module GameFamilyTrunfo
	# @author Diego Hernán Piccinini Lagos
	# validation to secure the inputs of the Game
	class ValidateGame
		class << self
			# Given two values String or Integer, checks whether they are valid in a context Game
			# then return the integers values or raise an exception
			# @param players [String|Integer]
			# @param cards_per_player [String|Integer]
			# @return players and cards per player [Array] with integers values of each one
			def parse_and_validate(players, cards_per_player)
				begin
					players = Integer(players)
					cards_per_player= Integer(cards_per_player)

				rescue
					raise TypeError
				end
				raise GameFamilyTrunfo::ToManyPlayersError if players > GameFamilyTrunfo::Game::MAXIMUN_PLAYERS
				raise GameFamilyTrunfo::NotEnoughPlayersError if players < GameFamilyTrunfo::Game::MINIMUN_PLAYERS
				raise GameFamilyTrunfo::ToManyCardsPerPlayerError if cards_per_player > GameFamilyTrunfo::Game::TOTAL_CARDS
				raise GameFamilyTrunfo::NotEnoughCardsPerPlayersError if cards_per_player < GameFamilyTrunfo::Game::MINIMUN_CARDS
				# raise GameFamilyTrunfo::TooManyCardsDemandedError if (players * cards_per_player) > GameFamilyTrunfo::Game::TOTAL_CARDS
				[players,cards_per_player]
			end

			# Validates whether the amount and type of players are right. Otherwise raise an exception
			# @param players [String|Integer]
			# @return number of players [Integer]
			def parse_and_validate_players(players)
				begin
					players = Integer(players)
				rescue
					raise TypeError
				end
				raise GameFamilyTrunfo::ToManyPlayersError if players > GameFamilyTrunfo::Game::MAXIMUN_PLAYERS
				raise GameFamilyTrunfo::NotEnoughPlayersError if players < GameFamilyTrunfo::Game::MINIMUN_PLAYERS
				players
			end
		end
	end
end