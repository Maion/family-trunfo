class Message < ApplicationRecord
  belongs_to :game
  belongs_to :player
  after_create_commit { MessageBroadcastJob.perform_later self }
end
