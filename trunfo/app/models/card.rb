class Card < ActiveRecord::Base
  #  belongs_to :deck_cards, dependent: :destroy
    validates_uniqueness_of :name
      include OrderQuery
  order_query :order_display, [
    [:updated_at, :desc],
    [:id, :desc]
  ]
end
