class Player < ActiveRecord::Base
  belongs_to :user
  belongs_to :game
  has_one :deck
  has_many :deck_cards, through: :deck#, dependent: :destroy #nop
  has_many :cards, through: :deck_cards# , dependent: :destroy #nop
  
    accepts_nested_attributes_for :deck, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :deck_cards, reject_if: :all_blank, allow_destroy: true
 #   accepts_nested_attributes_for :cards, reject_if: :all_blank, allow_destroy: true
 

end
